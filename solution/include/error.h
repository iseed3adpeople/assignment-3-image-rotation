#pragma once
#ifndef ERROR_H
#define ERROR_H

#define FOREACH_ERROR_CODES(NAME) \
        NAME(INVALID_ARGUMENTS)   \
        NAME(FILE_OPENING_ERROR)  \
        NAME(IMAGE_DESERIALIZING_ERROR) \
        NAME(IMAGE_SERIALIZING_ERROR)  \

#define GENERATE_ENUM(ENUM) ENUM,
#define GENERATE_STRING(STRING) #STRING,

enum error_codes {
    FOREACH_ERROR_CODES(GENERATE_ENUM)
};

static const char *error_codes_str[] = {
    FOREACH_ERROR_CODES(GENERATE_STRING)
};

#endif
