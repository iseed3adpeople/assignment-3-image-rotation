#pragma once
#ifndef BMP_H
#define BMP_H
#include  <stdint.h>

#define BMP_SIGNATURE_CODE 0x4D42
#define BMP_INFORMATION_HEADER_SIZE 40
#define BMP_COLOR_PLANES 1
#define BMP_COLOR_DEPTH 24
#define BMP_COMPRESSION 0
#define BMP_PELS_PER_METER 0xB12
#define BMP_CLR_USED 0
#define BMP_CLR_IMPORTANT 0


#pragma pack(push, 1)
struct bmp_header {
        uint16_t bfType;
        uint32_t bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t biHeight;
        uint16_t biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t biClrImportant;
};
#pragma pack(pop)

#define SET_HEADER_DEFAULT(header, width, height, padding, pixel_size) \
    (header).bfType = BMP_SIGNATURE_CODE;\
	(header).bfileSize = sizeof(struct bmp_header) + ((width) + (padding)) * (height) * (pixel_size);\
	(header).bfReserved = 0;\
	(header).bOffBits = sizeof(struct bmp_header);\
	(header).biSize = sizeof(struct bmp_header) - 14;\
	(header).biWidth = (width);\
	(header).biHeight = (height);\
	(header).biPlanes = BMP_COLOR_PLANES;\
	(header).biBitCount = BMP_COLOR_DEPTH;\
	(header).biCompression = BMP_COMPRESSION;\
	(header).biSizeImage = (width) * (height) * (pixel_size);\
	(header).biXPelsPerMeter = BMP_PELS_PER_METER;\
	(header).biYPelsPerMeter = BMP_PELS_PER_METER;\
	(header).biClrUsed = BMP_CLR_USED;\
	(header).biClrImportant = BMP_CLR_IMPORTANT;\


#endif
