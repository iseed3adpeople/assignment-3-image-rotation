#pragma once
#ifndef ROTATE_H
#define ROTATE_H
#include "image.h"


#define CONVERT_ANGLE(angle)  angle = (-(angle) + 360) % 360;

void rotate(struct image* img, int64_t angle);

#endif
