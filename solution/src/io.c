#include "bmp.h"
#include "io.h"

enum read_status from_bmp(FILE* in, struct image* img) {
    if (!in || !img) {
        return READ_INVALID_BITS;
    }

    struct bmp_header header;
    fread(&header, sizeof(struct bmp_header), 1, in);

    img->width = header.biWidth;
    img->height = header.biHeight;

    // Вычисление размера строки пикселей с учетом padding
    uint64_t row_size = img->width * sizeof(struct pixel);
    uint64_t padding = 4 - (header.biWidth * sizeof(struct pixel)) % 4;
    uint64_t padded_row_size = (row_size + 3) & ~3; // Добавление padding

    // Выделение памяти для хранения данных пикселей
    free(img->data);
    img->data = (struct pixel*)malloc(padded_row_size * img->height * sizeof(struct pixel));
    if (img->data == NULL) {
        return READ_INVALID_BITS;
    }

    if (fseek(in, (long)header.bOffBits, SEEK_SET) != 0) {
        free(img->data);
        return READ_INVALID_BITS;
    }

    for (size_t i = 0; i < img->height; ++i) {
        for (size_t j = 0; j < img->width; ++j) {
            struct pixel tmp;
            if (fread(&tmp, sizeof(struct pixel), 1, in) != 1) {
                free(img->data);
                return READ_INVALID_BITS;
            }
            img->data[j + i * img->width] = tmp;
            if (j == img->width - 1 && padding != 0) {
                if (fseek(in, (long)padding, SEEK_CUR) != 0) {
                    free(img->data);
                    return READ_INVALID_BITS;
                }
            }
        }
    }

    return READ_OK;
}


enum write_status to_bmp(FILE* out, struct image const* img) {
    if (!out || !img) {
        return WRITE_ERROR;
    }

    // Заголовок BMP
    struct bmp_header header;

    uint64_t bytes_to_write = img->width * sizeof(struct pixel);
    uint64_t padding = (4 - (bytes_to_write % 4)) % 4; // Вычисляем padding


    // Заполняем заголовок BMP данными из img
    SET_HEADER_DEFAULT(header, img->width, img->height, padding, sizeof(struct pixel));

    // Записываем заголовок в файл
    fwrite(&header, sizeof(struct bmp_header), 1, out);
    
    // Запись данных пикселей в файл
    for (uint64_t y = 0; y < img->height; ++y) {
        for (uint64_t x = 0; x < img->width; ++x) {
            if (fwrite(&img->data[y * img->width + x], sizeof(struct pixel), 1, out) != 1) {
                return WRITE_ERROR;
            }
        }
        for (int p = 0; p < padding; p++) {
            fputc(0, out);
        }
    }


    return WRITE_OK;
}
