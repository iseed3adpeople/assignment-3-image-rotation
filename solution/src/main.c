#include "error.h"
#include "io.h"
#include "rotate.h"

int main( int argc, char** argv ) {

    if (argc != 4) {
        fprintf(stderr, "Invalid arguments format\n");
        fprintf(stderr, "Correct order: <source-image> <transformed-image> <angle>\n");
        return INVALID_ARGUMENTS;
    }

    char* input_image_path = argv[1];
    char* output_image_path = argv[2];
    int64_t angle = strtoll(argv[3], NULL, 10);

    FILE* input_image = fopen(input_image_path, "rb");
    FILE* output_image = fopen(output_image_path, "wb");

    if (!input_image || !output_image) {
        fprintf(stderr, "Error opening the file\n");
        return FILE_OPENING_ERROR;
    }

    struct image img = {0};
    enum read_status read = from_bmp(input_image, &img);

    if (read != READ_OK) {
        fprintf(stderr, "%s\n", error_codes_str[read]);
        return IMAGE_DESERIALIZING_ERROR;
    }

    rotate(&img, angle);

    enum write_status write = to_bmp(output_image, &img);

    free(img.data);

    if (write != WRITE_OK){
        fprintf(stderr, "Error occured while writin to file\n");
        return IMAGE_SERIALIZING_ERROR;
    }
    
    fclose(input_image);
    fclose(output_image);

    return 0;
}
