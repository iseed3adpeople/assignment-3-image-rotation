#include "rotate.h"

//rotate by 90 angle
static void _rotate(struct image* img);


void rotate(struct image* img, int64_t angle){
    CONVERT_ANGLE(angle);

    for (int64_t i = 0; i < angle; i+=90) {
        _rotate(img);
    }
}

static void _rotate(struct image* img){
    // Создаем временное изображение для сохранения повернутых пикселей
    struct pixel* rotated_data = (struct pixel*)malloc(img->width * img->height * sizeof(struct pixel));
    if (rotated_data == NULL) {
        return; // Обработка ошибки выделения памяти
    }

    // Поворот изображения на 90 градусов по часовой стрелке
    for (uint64_t y = 0; y < img->height; y++) {
        for (uint64_t x = 0; x < img->width; x++) {
            rotated_data[x * img->height + (img->height - y - 1)] = img->data[y * img->width + x];
        }
    }

    // Обновляем ширину и высоту изображения
    uint64_t temp = img->width;
    img->width = img->height;
    img->height = temp;

    // Освобождаем старые данные и присваиваем новые повернутые данные
    free(img->data);
    img->data = rotated_data;
}
